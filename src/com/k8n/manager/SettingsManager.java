package com.k8n.manager;

import com.k8n.exception.SettingsException;
import com.k8n.resource.Resources;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SettingsManager extends Manager {

    private final String NAME = "settings.xml";
    private final String ROOT_NAME = "settings";

    private static SettingsManager instance;

    private File file;
    private Document document;
    private Node rootElement;
    private XPath xPath;

    private SettingsManager() throws SettingsException{
        File projectDir = getProjectDir();
        file = new File(projectDir, NAME);
        xPath = XPathFactory.newInstance().newXPath();

        try {
            if (file.exists()) {
                document = parseXMLDocumentFromFile(file);
                rootElement = document.getFirstChild();
            } else {
                document = createNewXMLDocument();
                rootElement = createRootElement(document);
            }
        } catch (ParserConfigurationException | IOException | SAXException e) {
            boolean deleted = file.delete();
            if (deleted) {
                throw new SettingsException(Resources.getInstance().getGuiString("settings.error.file"));
            }
        }
    }

    public static synchronized SettingsManager getInstance() throws SettingsException{
        if (instance == null) {
            instance = new SettingsManager();
        }
        return instance;
    }

    public void updateSettings(Map<String, Object> settings) {
        for (Map.Entry<String, Object> entry : settings.entrySet()) {
            String[] pathParts = entry.getKey().split("\\.");
            String xPathString = "/settings";
            Node parent = rootElement;

            for (String part : pathParts) {
                xPathString += "/" + part;
                Node tempNode = null;
                try {
                    tempNode = (Node) xPath.compile(xPathString).evaluate(document, XPathConstants.NODE);
                } catch (XPathExpressionException e) {
                    e.printStackTrace();
                }
                if (tempNode != null) {
                    parent = tempNode;
                } else {
                    Element newNode = document.createElement(part);
                    parent.appendChild(newNode);
                    parent = newNode;
                }
            }

            setValue(parent, entry.getValue());
        }
    }

    public void save() throws SettingsException{
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(file);
            transformer.transform(source, result);
        } catch (TransformerException e) {
            file.deleteOnExit();
            throw new SettingsException(Resources.getInstance().getGuiString("settings.error.save"));
        }
    }

    public String getStringSetting(String key) {
        String xPathString = covertToXpath(key);

        Node tempNode = null;
        try {
            tempNode = (Node) xPath.compile(xPathString).evaluate(document, XPathConstants.NODE);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        if (tempNode != null) {
            return tempNode.getTextContent();
        }

        return "";
    }

    public String[] getArraySetting(String key) {
        String xPathString = covertToXpath(key);

        Node tempNode = null;
        try {
            tempNode = (Node) xPath.compile(xPathString).evaluate(document, XPathConstants.NODE);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        if (tempNode != null) {
            int length = tempNode.getChildNodes().getLength();
            String[] items = new String[length];
            for (int i = 0; i < length; i++) {
                items[i] = tempNode.getChildNodes().item(i).getTextContent();
            }
            return items;
        }

        return new String[0];
    }

    private Document parseXMLDocumentFromFile(File file) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        return documentBuilder.parse(file);
    }

    private Document createNewXMLDocument() throws ParserConfigurationException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        return documentBuilder.newDocument();
    }

    private Element createRootElement(Document document) {
        Element rootElement = document.createElement(ROOT_NAME);
        document.appendChild(rootElement);
        return rootElement;
    }

    private void setValue(Node node, Object value) {
        if (value instanceof String) {
            node.setTextContent((String) value);
        } else if (value instanceof String[]) {
            while (node.hasChildNodes()) {
                node.removeChild(node.getFirstChild());
            }

            for (String string : (String[]) value) {
                Element element = document.createElement("item");
                element.setTextContent(string);
                node.appendChild(element);
            }
        } else {
            throw new IllegalArgumentException();
        }
    }

    private String covertToXpath(String key) {
        String[] pathParts = key.split("\\.");
        return  "/settings/" + String.join("/", pathParts);
    }

    public static void main(String... args) {
        try {
            SettingsManager manager = SettingsManager.getInstance();
            HashMap<String, Object> settings = new HashMap<>();
            settings.put("library.name", "library5");
//            settings.put("ignore.name", "ignore4");
            settings.put("library.paths", new String[] {"C:\\path10", "C:\\path11", "C:\\path12"});
            manager.updateSettings(settings);
            manager.save();

            System.out.println(manager.getStringSetting("library.name"));
            System.out.println(manager.getStringSetting("ignore.name"));
            System.out.println(Arrays.toString(manager.getArraySetting("library.paths")));
        } catch (SettingsException e) {
            e.printStackTrace();
        }

    }

}
