package com.k8n.manager;

import com.k8n.data.Book;
import com.k8n.data.Person;
import com.k8n.data.Series;
import com.k8n.scanner.reader.PathUtils;

import java.io.File;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBManager extends Manager {

    private final String URL;

    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
        File propertiesDir = getProjectDir();
        URL = "jdbc:sqlite:" + propertiesDir.getPath() + "\\database.db";
    }

    public void createDatabase() {
        try {
            Class.forName("org.sqlite.JDBC").newInstance();
        } catch (Exception ex) {
            // TODO
            // handle the error
        }

        try (Connection connection = DriverManager.getConnection(URL)) {
            if (connection != null) {
                System.out.println("Data bse should be created at " + URL);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void createTables() {
        try (Connection connection = DriverManager.getConnection(URL);
             Statement statement = connection.createStatement()) {

            String authorsTable =
                    "CREATE TABLE authors" +
                            "(\n" +
                            "id integer PRIMARY KEY, \n" +
                            "firstName text, \n" +
                            "middleName text, \n" +
                            "lastName text NOT NULL, \n" +
                            "country text, \n" +
                            "UNIQUE(firstName, lastName) ON CONFLICT IGNORE" +
                            ");";

            String translatorsTable =
                    "CREATE TABLE translators" +
                            "(\n" +
                            "id integer PRIMARY KEY, \n" +
                            "firstName text, \n" +
                            "middleName text, \n" +
                            "lastName text NOT NULL, \n" +
                            "UNIQUE(firstName, lastName) ON CONFLICT IGNORE" +
                            ");";

            String genresTable =
                    "CREATE TABLE genres" +
                            "(\n" +
                            "id integer PRIMARY KEY, \n" +
                            "name text, \n" +
                            "UNIQUE(name) ON CONFLICT IGNORE" +
                            ");";

            String seriesTable =
                    "CREATE TABLE series" +
                            "(\n" +
                            "id integer PRIMARY KEY, \n" +
                            "name text, \n" +
                            "amount integer, \n" +
                            "authorId integer, \n" +
                            "UNIQUE(name) ON CONFLICT IGNORE, \n"+
                            "FOREIGN KEY(authorId) REFERENCES authors(id) \n" +
                            ");";

            String formatsTable =
                    "CREATE TABLE formats" +
                            "(\n" +
                            "id integer PRIMARY KEY, \n" +
                            "name text, \n" +
                            "UNIQUE(name) ON CONFLICT IGNORE \n"+
                            ");";

            String pathsTable =
                    "CREATE TABLE paths" +
                            "(\n" +
                            "id integer PRIMARY KEY, \n" +
                            "path text, \n" +
                            "formatId integer, \n" +
                            "bookId integer, \n" +
                            "UNIQUE(path) ON CONFLICT REPLACE, \n"+
                            "FOREIGN KEY(formatId) REFERENCES formats(id) \n" +
                            ");";

            String booksTable =
                    "CREATE TABLE books" +
                            "(\n" +
                            "id integer PRIMARY KEY, \n" +
                            "title text NOT NULL, \n" +
                            "authorId integer, \n" +
                            "date text, \n" +
                            "translatorId integer, \n" +
                            "genreId integer, \n" +
                            "seriesId integer, \n" +
                            "seriesNum integer, \n" +
                            "UNIQUE(title, authorId) ON CONFLICT IGNORE, \n"+
                            "FOREIGN KEY(authorId) REFERENCES authors(id), \n" +
                            "FOREIGN KEY(translatorId) REFERENCES translators(id), \n" +
                            "FOREIGN KEY(genreId) REFERENCES genres(id), \n" +
                            "FOREIGN KEY(seriesId) REFERENCES series(id) \n" +
                            ");";

            statement.execute("DROP TABLE IF EXISTS books");
            statement.execute("DROP TABLE IF EXISTS authors");
            statement.execute("DROP TABLE IF EXISTS translators");
            statement.execute("DROP TABLE IF EXISTS genres");
            statement.execute("DROP TABLE IF EXISTS series");
            statement.execute("DROP TABLE IF EXISTS formats");
            statement.execute("DROP TABLE IF EXISTS paths");
            statement.execute(authorsTable);
            statement.execute(translatorsTable);
            statement.execute(genresTable);
            statement.execute(seriesTable);
            statement.execute(formatsTable);
            statement.execute(pathsTable);
            statement.execute(booksTable);
        } catch (SQLException e) {
            //TODO
            System.out.println(e.getMessage());
        }
    }



    public static void main(String... args) {
        DBManager dbManager = new DBManager();
        dbManager.createDatabase();
        dbManager.createTables();
    }

    public void saveBooks(List<Book> books) {
        // TODO: transaction
        try {
            for (Book book : books) {
                System.out.println("book = " + book);

                if (book.getAuthor().getId() < 0) {
                    book.getAuthor().setId(saveAuthor(book.getAuthor()));
                }

                if (!book.getTranslator().isEmpty() && book.getTranslator().getId() < 0) {
                    book.getTranslator().setId(saveTranslator(book.getTranslator()));
                }

                int genreId = saveGenre(book.getGenre());

                if (book.getSeries() != null && book.getSeries().getId() < 0) {
                    book.getSeries().setId(saveSeries(book.getSeries()));
                }

                if (book.getId() < 0) {
                    String sql = "INSERT INTO books(title, authorId, date," +
                            "translatorId, genreId, seriesId, seriesNum) VALUES(?, ?, ?, ?, ?, ?, ?)";

                    try (Connection connection = this.connect();
                         PreparedStatement pstmt = connection.prepareStatement(sql)) {
                        pstmt.setString(1, book.getTitle());
                        pstmt.setInt(2, book.getAuthor().getId());
                        pstmt.setString(3, book.getDate());
                        pstmt.setInt(4, book.getTranslator().getId());
                        pstmt.setInt(5, genreId);
                        if (book.getSeries() != null) {
                            pstmt.setInt(6, book.getSeries().getId());
                        } else {
                            pstmt.setInt(6, -1);
                        }
                        pstmt.setInt(7, book.getNumIsSeries());
                        pstmt.executeUpdate();
                    }
                    book.setId(getBookId(book));
                }

                savePath(book);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private void saveBookPath(Book book, int pathId) throws SQLException {
        String sql = "INSERT INTO book_paths(pathId, bookId) VALUES(?, ?)";

        try (Connection connection = this.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, pathId);
            preparedStatement.setInt(2, book.getId());
            preparedStatement.executeUpdate();
        }
    }

    private int getBookId(Book book) throws SQLException {
        String sql = "SELECT id FROM books WHERE title = ? AND authorId = ?";
        try (Connection connection = this.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, book.getTitle());
            preparedStatement.setInt(2, book.getAuthor().getId());

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("id");
            }
        }

        return -1;
    }

    private int savePath(Book book) throws SQLException {
        int pathId = getPathId(book.getPath());
        if (pathId > -1) {
            return pathId;
        }

        String format = PathUtils.getExtension(Paths.get(book.getPath()));
        int formatId = saveFormat(format);

        String sql = "INSERT INTO paths(path, formatId, bookId) VALUES(?, ?, ?)";

        try (Connection connection = this.connect();
             PreparedStatement pstmt = connection.prepareStatement(sql)) {
            pstmt.setString(1, book.getPath());
            pstmt.setInt(2, formatId);
            pstmt.setInt(3, book.getId());
            pstmt.executeUpdate();
        }

        return getPathId(book.getPath());
    }

    private int saveFormat(String format) throws SQLException {
        int formatId = getFormatId(format);
        if (formatId > -1) {
            return formatId;
        }

        String sql = "INSERT INTO formats(name) VALUES(?)";

        try (Connection connection = this.connect();
             PreparedStatement pstmt = connection.prepareStatement(sql)) {
            pstmt.setString(1, format);
            pstmt.executeUpdate();
        }

        return getFormatId(format);
    }

    private int getFormatId(String format) throws SQLException {
        String sql = "SELECT id FROM formats WHERE name = ?";
        try (Connection connection = this.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, format);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("id");
            }
        }

        return -1;
    }

    private int getPathId(String path) throws SQLException {
        String sql = "SELECT id FROM paths WHERE path = ?";
        try (Connection connection = this.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, path);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("id");
            }
        }

        return -1;
    }

    private int saveSeries(Series series) throws SQLException {
        String sql = "INSERT INTO series(name) VALUES(?)";

        try (Connection connection = this.connect();
             PreparedStatement pstmt = connection.prepareStatement(sql)) {
            pstmt.setString(1, series.getName());
            pstmt.executeUpdate();
        }

        return getSeriesId(series);
    }

    private int getSeriesId(Series series) throws SQLException {
        String sql = "SELECT id FROM series WHERE name = ?";
        try (Connection connection = this.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, series.getName());

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("id");
            }
        }

        return -1;

    }

    private int saveGenre(String genre) throws SQLException {
        int genreId = getGenreId(genre);
        if (genreId > 0) {
            return genreId;
        }

        String sql = "INSERT INTO genres(name) VALUES(?)";

        try (Connection connection = this.connect();
             PreparedStatement pstmt = connection.prepareStatement(sql)) {
            pstmt.setString(1, genre);
            pstmt.executeUpdate();
        }

        return getGenreId(genre);
    }

    private int getGenreId(String genre) throws SQLException {
        String sql = "SELECT id FROM genres WHERE name = ?";
        try (Connection connection = this.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, genre);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("id");
            }
        }

        return -1;
    }

    private Connection connect() throws SQLException {
        return DriverManager.getConnection(URL);
    }

    private int saveAuthor(Person author) throws SQLException {
        String sql = "INSERT INTO authors(firstName,middleName,lastName) VALUES(?,?,?)";

        try (Connection connection = this.connect();
             PreparedStatement pstmt = connection.prepareStatement(sql)) {
            pstmt.setString(1, author.getFirstName());
            pstmt.setString(2, author.getMiddleName());
            pstmt.setString(3, author.getLastName());
            pstmt.executeUpdate();
        }

        return getAuthorId(author);
    }

    private int getAuthorId(Person author) throws SQLException {
        String sql = "SELECT id FROM authors WHERE firstName = ? AND lastName = ?";
        try (Connection connection = this.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, author.getFirstName());
            preparedStatement.setString(2, author.getLastName());

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("id");
            }
        }

        return -1;
    }

    private int saveTranslator(Person translator) throws SQLException {
        String sql = "INSERT INTO translators(firstName,middleName,lastName) VALUES(?,?,?)";

        try (Connection connection = this.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, translator.getFirstName());
            preparedStatement.setString(2, translator.getMiddleName());
            preparedStatement.setString(3, translator.getLastName());
            preparedStatement.executeUpdate();
        }

        return getTranslatorId(translator);
    }

    private int getTranslatorId(Person author) throws SQLException {
        String sql = "SELECT id FROM translators WHERE firstName = ? AND lastName = ?";
        try (Connection connection = this.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, author.getFirstName());
            preparedStatement.setString(2, author.getLastName());

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("id");
            }
        }

        return -1;
    }

    public List<Person> getAuthors() throws SQLException {
        String sql = "SELECT *  FROM authors";
        List<Person> results = new ArrayList<>();
        try (Connection connection = this.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Person person = new Person();
                person.setId(resultSet.getInt("id"));
                person.setFirstName(resultSet.getString("firstName"));
                person.setMiddleName(resultSet.getString("middleName"));
                person.setLastName(resultSet.getString("lastName"));
                results.add(person);
            }
        }
        return results;
    }

    public List<Person> getTranslators() throws SQLException {
        String sql = "SELECT *  FROM translators";
        List<Person> results = new ArrayList<>();
        try (Connection connection = this.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Person person = new Person();
                person.setId(resultSet.getInt("id"));
                person.setFirstName(resultSet.getString("firstName"));
                person.setMiddleName(resultSet.getString("middleName"));
                person.setLastName(resultSet.getString("lastName"));
                results.add(person);
            }
        }
        return results;
    }

    public List<String> getGenres() throws SQLException {
        String sql = "SELECT *  FROM genres";
        List<String> results = new ArrayList<>();
        try (Connection connection = this.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
//                person.setId(resultSet.getInt("id"));
                results.add(resultSet.getString("name"));
            }
        }
        return results;
    }

    public List<Series> getSeries() throws SQLException {
        String sql = "SELECT *  FROM series";
        List<Series> results = new ArrayList<>();
        try (Connection connection = this.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Series series = new Series();
                series.setId(resultSet.getInt("id"));
                series.setName(resultSet.getString("name"));
                series.setAmount(resultSet.getInt("amount"));
                series.setAuthorId(resultSet.getInt("authorId"));
                results.add(series);
            }
        }
        return results;
    }

    public List<String> getTitles() throws SQLException {
        String sql = "SELECT *  FROM books";
        List<String> results = new ArrayList<>();
        try (Connection connection = this.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                results.add(resultSet.getString("title"));
            }
        }
        return results;
    }
}
