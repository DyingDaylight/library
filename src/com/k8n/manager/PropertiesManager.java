package com.k8n.manager;

import java.io.*;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

public class PropertiesManager extends Manager{

    private static final String LANGUAGE = "language";

    private static PropertiesManager instance;

    private Properties properties;
    private File propertiesFile;

    private PropertiesManager() {
        File propertiesDir = getProjectDir();
        propertiesFile = new File(propertiesDir, "program.properties");

        Properties defaultProperties = new Properties();
        defaultProperties.setProperty(LANGUAGE, Locale.getDefault().getLanguage());

        properties = new Properties(defaultProperties);

        if (propertiesFile.exists()) {
            try (InputStream in = new FileInputStream(propertiesFile)) {
                properties.load(in);
            } catch (IOException e) {
                Logger.getGlobal().warning(e.getMessage());
                propertiesFile.deleteOnExit();
            }
        }
    }

    public static synchronized PropertiesManager getInstance() {
        if (instance == null) {
            instance = new PropertiesManager();
        }
        return instance;
    }

    public void setLanguage(String language) {
        properties.setProperty(LANGUAGE, language);
    }

    public String getLanguage() {
        return properties.getProperty(LANGUAGE);
    }

    public void save() {
        try (OutputStream out = new FileOutputStream(propertiesFile)) {
            properties.store(out, "Program Properties");
        } catch (IOException e) {
            Logger.getGlobal().warning(e.getMessage());
            propertiesFile.deleteOnExit();
        }
    }

    public void addProperties(Map<String, String> stateMemento) {
        for (Map.Entry<String, String> entry : stateMemento.entrySet()) {
            properties.setProperty(entry.getKey(), entry.getValue());
        }
    }

    public HashMap<String, String> getProperties(String group) {
        HashMap<String, String> stateMemento = new HashMap<>();
        for (String name : properties.stringPropertyNames()) {
            if (name.startsWith(group)) {
                stateMemento.put(name, properties.getProperty(name));
            }
        }
        return stateMemento;
    }
}
