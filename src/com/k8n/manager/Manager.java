package com.k8n.manager;

import java.io.File;

public abstract class Manager {

    protected File getProjectDir() {
        String userDir = System.getProperty("user.home");
        File propertiesDir = new File(userDir, ".library");
        if (!propertiesDir.exists()) {
            propertiesDir.mkdir();
        }
        return propertiesDir;
    }
}
