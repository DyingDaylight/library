package com.k8n.scanner;

import com.k8n.data.Book;
import com.k8n.data.Person;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import java.util.List;
import java.util.ArrayList;

public class ScannedBooksTableModel extends AbstractTableModel {

    enum Columns {

        CHECK("", Boolean.class),
        AUTHOR("Author"),
        TITLE("Title"),
        YEAR("Year"),
        TRANSLATOR("Translator"),
        GENRE("Genre"),
        PATH("Path"),
        SERIES("Series");

        private String name = "";
        private Class aClass = String.class;

        Columns(String name) {
            this(name, String.class);
        }

        Columns(String name, Class aClass) {
            this.name = name;
            this.aClass = aClass;
        }

        public String getName() {
            return name;
        }

        public Class getAClass() {
            return aClass;
        }
    }

    private ScannedBooksModel model;

    private List<Integer> selectedIntegers = new ArrayList<>();

    ScannedBooksTableModel(ScannedBooksModel model) {
        this.model = model;
        addTableModelListener(new BookTableModelListener());
    }

    public List<Book> getCheckedBooks() {
        List<Book> checkedBooks = new ArrayList<>();
        for (Integer index : selectedIntegers) {
            checkedBooks.add(model.getBookAt(index));
        }
        return checkedBooks;
    }

    @Override
    public int getRowCount() {
        if (model == null) {
            return 0;
        }
        return model.getBooksSize();
    }

    @Override
    public int getColumnCount() {
        return Columns.values().length;
    }

    @Override
    public String getColumnName(int column) {
        return Columns.values()[column].getName();
    }

    @Override
    public Class<?> getColumnClass(int column) {
        return Columns.values()[column].getAClass();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        switch (Columns.values()[columnIndex]){
            case PATH:
                return false;
            default:
                return true;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex < 0 || rowIndex < 0) {
            return null;
        }

        Book book = model.getBookAt(rowIndex);
        switch (Columns.values()[columnIndex]){
            case CHECK:
                return selectedIntegers.contains(rowIndex);
            case AUTHOR:
                return book.getAuthor().toString();
            case TITLE:
                return book.getTitle();
            case YEAR:
                return book.getDate();
            case GENRE:
                return book.getGenre();
            case TRANSLATOR:
                return book.getTranslator().toString();
            case PATH:
                return book.getPath();
            case SERIES:
                if (book.getSeries() != null) {
                    return book.getSeries().getName() + "  #" + book.getNumIsSeries();
                } else {
                    return "";
                }
            default:
                return "default";
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        System.out.println("tableChanged");
        if (columnIndex < 0 || rowIndex < 0) {
            return;
        }

        Book book = model.getBookAt(rowIndex);
        switch (Columns.values()[columnIndex]){
            case CHECK:
                if ((Boolean) aValue && !selectedIntegers.contains(rowIndex)) {
                    selectedIntegers.add(rowIndex);
                } else if (! (Boolean) aValue && selectedIntegers.contains(rowIndex)) {
                    selectedIntegers.remove(selectedIntegers.indexOf(rowIndex));
                }
                break;
            case AUTHOR:
                book.setAuthor(Person.parseString((String) aValue));
                break;
            case TITLE:
                book.setTitle((String) aValue);
                break;
            case YEAR:
                book.setDate((String) aValue);
                break;
            case GENRE:
                book.setGenre((String) aValue);
                break;
            case TRANSLATOR:
                book.setTranslator(Person.parseString((String) aValue));
                break;
            default:
                return;
        }
        fireTableCellUpdated(rowIndex, columnIndex);
    }

    private class BookTableModelListener implements TableModelListener {

        @Override
        public void tableChanged(TableModelEvent e) {


        }
    }
}
