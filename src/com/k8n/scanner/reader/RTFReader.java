package com.k8n.scanner.reader;

import com.k8n.data.Book;
import com.k8n.data.Person;

import java.io.*;
import java.nio.file.Path;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RTFReader extends Reader {

    private static final String EXTENSION = "rtf";

    public static boolean isApplicable(Path path) {
        String extension = PathUtils.getExtension(path);
        return extension.equalsIgnoreCase(EXTENSION);
    }

    private enum Regexps {

        BOOK_TITLE(".*\\{\\\\title (.+?)\\}.*"),
        AUTHOR_NAME(".*\\{\\\\author (.+?)\\}.*");

        private String regexp = "";

        private Regexps(String regexp) {
            this.regexp = regexp;
        }

        protected String getRegexp() {
            return regexp;
        }
    }

    @Override
    public Book parse(Path path) {
        Book book = new Book();

        try {
//            BufferedReader bufferedReader = new BufferedReader(new FileReader(path.toFile()));
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(path.toFile()), "windows-1251"));

            Pattern titlePattern = Pattern.compile(Regexps.BOOK_TITLE.getRegexp());
            Pattern authorPattern = Pattern.compile(Regexps.AUTHOR_NAME.getRegexp());

            String line;
            Matcher matcher;
            while((line = bufferedReader.readLine()) != null) {
                if (! book.isEmpty()) {
                    break;
                }

                matcher = titlePattern.matcher(line);
                if (matcher.matches()) {
                    book.setTitle(matcher.group(1).trim());
                }

                matcher = authorPattern.matcher(line);
                if (matcher.matches()) {
                    String authorValue = matcher.group(1);
                    if (authorValue != null) {
                        authorValue = authorValue.trim();
                        Person author = Person.parseString(authorValue);
                        book.setAuthor(author);
                    }
                }
            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
            Logger.getGlobal().warning(e.getMessage());
        }
        return book;
    }
}
