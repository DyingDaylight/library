package com.k8n.scanner.reader;

import com.k8n.exception.SettingsException;

import java.nio.file.Path;

public class ReaderFactory {

    public static Reader createReader(Path path) {
        try {
            if (IgnoreReader.isApplicable(path)) {
                return new IgnoreReader();
            } else if (GenericReader.isApplicable(path)) {
                return new GenericReader();
            } else if (TxtReader.isApplicable(path)) {
                return new TxtReader();
            } else if (ZIPReader.isApplicable(path)) {
                return new ZIPReader();
            } else if (FB2Reader.isApplicable(path)) {
                return new FB2Reader();
            } else if (RTFReader.isApplicable(path)) {
                return new RTFReader();
            } else {
                //TODO
                System.out.println(String.format("\t !!!! Unknown file type %s !!!!", path.toString()));
            }
        } catch (SettingsException e) {
            //TODO
            System.out.println(String.format("\t !!!! Cannot get ignore list !!!!", path.toString()));
        }
        return new Reader();
    }
}
