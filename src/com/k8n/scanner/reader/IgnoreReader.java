package com.k8n.scanner.reader;

import com.k8n.data.Book;
import com.k8n.exception.SettingsException;
import com.k8n.manager.SettingsManager;
import com.k8n.settings.settingsItem.IgnoreSettingsItemController;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class IgnoreReader extends Reader {

    private static final List<String> supportedExtensions = new ArrayList<String>() {{
        add("gif");
        add("css");
        add("js");
        add("js-v3");
        add("php");
        add("png");
        add("jpg");
        add("xml");
        add("gi_");
        add("jp_");
    }};

    public static boolean isApplicable(Path path) throws SettingsException {
        SettingsManager manager = SettingsManager.getInstance();
        String[] items = manager.getArraySetting(IgnoreSettingsItemController.PATHS);
        String pathString = path.toString();
        for (String item : items) {
            try {
                Pattern pattern = Pattern.compile(item);
                Matcher matcher = pattern.matcher(pathString);
                if (matcher.find()){
                    return true;
                }
            } catch (PatternSyntaxException e) {
                Logger.getGlobal().warning(String.format("The pattern %s has been skipped", item));
            }
        }
        return false;
    }

    @Override
    public Book parse(Path path) {
        return Book.getEmptyBook();
    }
}
