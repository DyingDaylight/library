package com.k8n.scanner.reader;

import com.k8n.data.Book;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZIPReader extends Reader {

    private static final String EXTENSION = "zip";

    public static boolean isApplicable(Path path) {
        String extension = PathUtils.getExtension(path);
        return extension.equalsIgnoreCase(EXTENSION);
    }

    @Override
    public Book parse(Path path) throws IOException {
        Book book = new Book();
        byte[] buffer = new byte[1024];
        ZipInputStream zis = new ZipInputStream(new FileInputStream(path.toFile()));
        ZipEntry zipEntry;
        while ((zipEntry = zis.getNextEntry()) != null){
            String currentDir = System.getProperty("user.dir");
            File newFile = File.createTempFile("book",
                    "." + PathUtils.getExtension(Paths.get(zipEntry.getName())),
                    new File(currentDir));
            newFile.deleteOnExit();
            FileOutputStream fos = new FileOutputStream(newFile);
            int len;
            while ((len = zis.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
            }
            fos.close();

            Path subPath = Paths.get(newFile.toURI());
            Reader reader = ReaderFactory.createReader(subPath);
            book = reader.parse(subPath);
        }
        zis.closeEntry();
        zis.close();

        return book;
    }
}
