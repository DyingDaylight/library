package com.k8n.scanner.reader;

import com.k8n.data.Book;
import com.k8n.data.Person;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GenericReader extends Reader {

    private static final List<String> supportedExtensions = new ArrayList<String>() {{
        add("doc");
        add("docx");
        add("pdf");
        add("djvu");
        add("mobi");
        add("epub");
        add("html");
        add("htm");
        add("mht");
        add("exe");
    }};

    public static boolean isApplicable(Path path) {
        String extension = PathUtils.getExtension(path);
        return supportedExtensions.contains(extension.toLowerCase());
    }

    @Override
    public Book parse(Path path) {
        Book book = new Book();

        Pattern pattern1 = Pattern.compile("((?!\\d{4}).+?)\\s?-\\s?(.+)");
        Pattern pattern2 = Pattern.compile("(.+?)\\s?\\.\\s?(.+)");
        Pattern pattern3 = Pattern.compile(".*\\\\(.*)\\\\(.*)\\.\\w+$");

        String fileName = PathUtils.getFileNameWithoutExtension(path);
        Matcher matcher = pattern1.matcher(fileName);
        if (matcher.find()) {
            book.setAuthor(Person.parseString(matcher.group(1)));
            book.setTitle(matcher.group(2));
        } else if ((matcher = pattern2.matcher(fileName)).find()) {
            book.setAuthor(Person.parseString(matcher.group(1)));
            book.setTitle(matcher.group(2));
        } else if ((matcher = pattern3.matcher(path.toString())).find()) {
            book.setAuthor(Person.parseString(matcher.group(1)));
            book.setTitle(matcher.group(2));
        }
        return book;
    }

}
