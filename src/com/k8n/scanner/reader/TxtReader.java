package com.k8n.scanner.reader;

import com.k8n.data.Book;
import com.k8n.data.Person;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.util.logging.Logger;

public class TxtReader extends Reader {
    private static final String EXTENSION = "txt";

    public static boolean isApplicable(Path path) {
        String extension = PathUtils.getExtension(path);
        return extension.equalsIgnoreCase(EXTENSION);
    }

    @Override
    public Book parse(Path path) {
        Book book = new Book();

        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(path.toFile()), "windows-1251"));

            String line;
            int count = 0;
            while((line = bufferedReader.readLine()) != null) {
                if (! book.isEmpty()) {
                    break;
                }

                line = line.trim();

                if (! line.isEmpty() && count == 0) {
                     book.setTitle(line.trim());
                     count++;
                     continue;
                }

                if (! line.isEmpty() && count == 1) {
                    Person author = Person.parseString(line);
                    book.setAuthor(author);
                    count++;
                    continue;
                }

            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
            Logger.getGlobal().warning(e.getMessage());
        }
        return book;
    }
}
