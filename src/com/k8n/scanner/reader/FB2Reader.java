package com.k8n.scanner.reader;

import com.k8n.data.Book;
import com.k8n.data.Person;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.IOException;
import java.nio.file.Path;
import java.util.logging.Logger;

public class FB2Reader extends Reader {
    private static final String EXTENSION = "fb2";

    public static boolean isApplicable(Path path) {
        String extension = PathUtils.getExtension(path);
        return extension.equalsIgnoreCase(EXTENSION);
    }

    private enum Paths {

        GENRE("/FictionBook/description/title-info/genre"),
        BOOK_TITLE("/FictionBook/description/title-info/book-title"),
        AUTHOR_FIRST_NAME("/FictionBook/description/title-info/author/first-name"),
        AUTHOR_MIDDLE_NAME("/FictionBook/description/title-info/author/middle-name"),
        AUTHOR_LAST_NAME("/FictionBook/description/title-info/author/last-name"),
        DATE("/FictionBook/description/title-info/date"),
        TRANSLATOR_FIRST_NAME("/FictionBook/description/title-info/translator/first-name"),
        TRANSLATOR_LAST_NAME("/FictionBook/description/title-info/translator/last-name");

        private String path = "";

        private Paths(String path) {
            this.path = path;
        }

        protected String getPath() {
            return path;
        }
    }


    @Override
    public Book parse(Path path) {
        Book book = new Book();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(path.toFile());

            XPathFactory xPathFactory = XPathFactory.newInstance();
            XPath xPath = xPathFactory.newXPath();

            String bookTitle = getElement(document, xPath, Paths.BOOK_TITLE);
            book.setTitle(bookTitle);

            String date = getElement(document, xPath, Paths.DATE);
            book.setDate(date);

            String genre = getElement(document, xPath, Paths.GENRE);
            book.setGenre(genre);

            Person author = new Person();
            author.setFirstName(getElement(document, xPath, Paths.AUTHOR_FIRST_NAME));
            author.setMiddleName(getElement(document, xPath, Paths.AUTHOR_MIDDLE_NAME));
            author.setLastName(getElement(document, xPath, Paths.AUTHOR_LAST_NAME));
            book.setAuthor(author);

            Person translator = new Person();
            translator.setFirstName(getElement(document, xPath, Paths.TRANSLATOR_FIRST_NAME));
            translator.setLastName(getElement(document, xPath, Paths.TRANSLATOR_LAST_NAME));
            book.setTranslator(translator);

        } catch (ParserConfigurationException | IOException | SAXException | XPathExpressionException e) {
            e.printStackTrace();
            Logger.getGlobal().warning(e.getMessage());
        }

        return book;
    }

    private String getElement(Document document, XPath xPath, Paths key) throws XPathExpressionException {
        XPathExpression expression = xPath.compile(key.getPath());
        Object object = expression.evaluate(document, XPathConstants.STRING);
        if (object instanceof String) {
            return (String) object;
        }
        return "";
    }
}
