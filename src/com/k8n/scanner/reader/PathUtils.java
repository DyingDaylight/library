package com.k8n.scanner.reader;

import java.nio.file.Path;

public class PathUtils {

    public static String getExtension(Path path) {
        String extension = "";

        String fileName = path.getFileName().toString();
        int index = fileName.lastIndexOf('.');
        if (index >= 0) {
            extension = fileName.substring(index + 1).toLowerCase();
        }

        return extension;
    }

    public static String getFileNameWithoutExtension(Path path) {
        String fileName = path.getFileName().toString();
        int index = fileName.lastIndexOf('.');
        if (index >= 0) {
            fileName = fileName.substring(0, index);
        }

        return fileName;
    }
}
