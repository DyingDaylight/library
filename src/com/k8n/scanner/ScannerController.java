package com.k8n.scanner;

import com.k8n.manager.DBManager;
import com.k8n.data.Book;
import com.k8n.scanner.reader.Reader;
import com.k8n.scanner.reader.ReaderFactory;

import javax.swing.*;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public class ScannerController {

    private ScannerView view;
    private ScannedBooksModel model;

    private LibraryScanner libraryScanner;

    public ScannerController() {
        model = new ScannedBooksModel();
        view = new ScannerView(this, model);
    }

    public void scan(List<Path> paths) {
        view.startScan();

        libraryScanner = new LibraryScanner(paths);
        libraryScanner.execute();
    }

    public void cancelScanner() {
        if (libraryScanner != null) {
            libraryScanner.cancel(true);
        }
    }

    public void saveChecked() {
        List<Book> checkedBooks = view.getCheckedBooks();
        DBManager dbManager = DBManager.getInstance();
        dbManager.saveBooks(checkedBooks);

        try {
            model.setAuthors(dbManager.getAuthors());
            model.setTranslators(dbManager.getTranslators());
            model.setTitles(dbManager.getTitles());
            model.setSeries(dbManager.getSeries());
            model.setGenres(dbManager.getGenres());
            model.updateVariantsListeners();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Scans books in background thread and send requests to update table in GUI with the scanned books.
     */
    private class LibraryScanner extends SwingWorker<Void, Book> {

        List<Path> paths;

        private LibraryScanner(List<Path> paths) {
            this.paths = paths;
        }

        @Override
        protected Void doInBackground() throws Exception {
            for (Path path : paths) {
                System.out.println(path);
                Stream<Path> stream =  Files.walk(path, FileVisitOption.FOLLOW_LINKS);
                Iterator<Path> iterator = stream.iterator();
                while (iterator.hasNext()) {
                    if (isCancelled()) {
                        return null;
                    }

                    Path currentPath = iterator.next();
                    Book book = processFile(currentPath);
                    if (book != null && !book.isEmpty()) {
                        String relativePath = path.toUri().relativize(currentPath.toUri()).getPath();
                        book.setPath(relativePath);
                        publish(book);
                    }
                }
            }
            return null;
        }

        @Override
        protected void process(List<Book> chunks) {
            model.addBooks(chunks);
        }

        @Override
        protected void done() {
            super.done();
            view.endScan();
        }

        private Book processFile(Path path) throws IOException {
            if (!Files.isDirectory(path, LinkOption.NOFOLLOW_LINKS)) {
                Reader reader = ReaderFactory.createReader(path);
                return reader.parse(path);
            }
            return null;
        }
    }
}
