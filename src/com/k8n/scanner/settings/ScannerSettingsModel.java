package com.k8n.scanner.settings;

import java.util.ArrayList;
import java.util.List;

public class ScannerSettingsModel {

    private List<ScannerSettingsListener> listeners = new ArrayList<>();

    private String[] items;

    public ScannerSettingsModel() {
        items = new String[0];
        listeners = new ArrayList<>();
    }

    public void setItems(String[] items) {
        this.items = items;
        updateListeners();
    }

    public String[] getItems() {
        return items;
    }

    void addListener(ScannerSettingsListener listener) {
        if (! listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    private void updateListeners() {
        for (ScannerSettingsListener listener : listeners) {
            listener.updated();
        }
    }
}
