package com.k8n.scanner.settings;

public interface ScannerSettingsListener {

    void updated();
}
