package com.k8n.scanner.settings;

import com.k8n.exception.SettingsException;
import com.k8n.manager.SettingsManager;
import com.k8n.resource.Resources;
import com.k8n.scanner.ScannerController;

import java.util.List;
import java.nio.file.Path;

public class ScannerSettingsController {

    private static final String PATHS = "library.paths";

    private ScannerSettingsModel model;
    private ScannerSettingsView view;

    public ScannerSettingsController() {
        model = new ScannerSettingsModel();
        view = new ScannerSettingsView(this, model);
        try {
            loadData();
        } catch (SettingsException e) {
            e.printStackTrace();
        }
    }

    public void approved() {
        List<Path> paths = view.getLibraries();
        if (paths.isEmpty()) {
            view.showError(Resources.getInstance().getGuiString("scanner.settings.empty.error"));
            return;
        }
        ScannerController scannerController = new ScannerController();
        scannerController.scan(paths);
    }

    private void loadData() throws SettingsException {
        SettingsManager manager = SettingsManager.getInstance();
        String[] items = manager.getArraySetting(PATHS);
        model.setItems(items);
    }
}
