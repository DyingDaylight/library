package com.k8n.scanner.settings;

import com.k8n.BaseView;
import com.k8n.view.FileChooser;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ScannerSettingsView extends BaseView implements ScannerSettingsListener {

    private static final String PREFIX = "scanner.settings.window";

    private ScannerSettingsController controller;
    private ScannerSettingsModel model;

    private JPanel librariesPanel;
    private JButton cancelButton;
    private JButton okButton;

    private JCheckBox customLibrary;

    private List<JCheckBox> checkboxList = new ArrayList<>();

    private boolean isApproved = false;

    ScannerSettingsView(ScannerSettingsController controller, ScannerSettingsModel model) {
        this.controller = controller;
        this.model = model;
        this.model.addListener(this);

        populateFrame();
        setText();
        showFrame();
    }

    @Override
    public String getPrefix() {
        return PREFIX;
    }

    @Override
    protected void populateFrame() {
        Dimension gapDimension = new Dimension(GAP, GAP);

        // Create main panel
        BorderLayout borderLayout = new BorderLayout();
        borderLayout.setVgap(GAP);

        JPanel panel = new JPanel(borderLayout);
        panel.setBorder(new EmptyBorder(GAP, GAP, GAP, GAP));

        librariesPanel = new JPanel();
        BoxLayout librariesLayout = new BoxLayout(librariesPanel, BoxLayout.Y_AXIS);
        librariesPanel.setLayout(librariesLayout);
        JScrollPane scrollPane = new JScrollPane(librariesPanel);

        panel.add(scrollPane, BorderLayout.CENTER);

        // Create a panel with control buttons
        JPanel controlPanel = new JPanel();
        BoxLayout boxLayout = new BoxLayout(controlPanel, BoxLayout.X_AXIS);
        controlPanel.setLayout(boxLayout);

        controlPanel.add(Box.createHorizontalGlue());

        okButton = new JButton();
        controlPanel.add(okButton);
        okButton.addActionListener(e -> {
            isApproved = true;
            closeFrame();
            controller.approved();
        });

        controlPanel.add(Box.createRigidArea(gapDimension));

        cancelButton = new JButton();
        controlPanel.add(cancelButton);
        cancelButton.addActionListener(e -> closeFrame());

        panel.add(controlPanel, BorderLayout.SOUTH);

        add(panel);
    }

    @Override
    protected void setText() {
        assert SwingUtilities.isEventDispatchThread();

        setTitle(resources.getGuiString("title"));

        cancelButton.setText(resources.getGuiString("cancel"));
        okButton.setText(resources.getGuiString("ok"));

        if (customLibrary != null) {
            customLibrary.setText(resources.getGuiString("scanner.settings.choose.custom"));
        }
    }

    @Override
    public void updated() {
        String[] libraries = model.getItems();
        for (String library : libraries) {
            JComponent libraryPanel = createLibraryComponent(library);
            librariesPanel.add(libraryPanel);
        }

        JPanel libraryPanel = new JPanel();
        BoxLayout libraryLayout = new BoxLayout(libraryPanel, BoxLayout.X_AXIS);
        libraryPanel.setLayout(libraryLayout);

        customLibrary = new JCheckBox(resources.getGuiString("scanner.settings.choose.custom"));
        customLibrary.setEnabled(false);
        libraryPanel.add(customLibrary);
        libraryPanel.add(Box.createHorizontalGlue());

        checkboxList.add(customLibrary);

        JButton chooseButton = new JButton("...");
        chooseButton.addActionListener(e -> chooseDirectory());
        libraryPanel.add(chooseButton);

        librariesPanel.add(libraryPanel);
    }

    public List<Path> getLibraries() {
        ArrayList<Path> libraries = new ArrayList<>();
        for (JCheckBox checkBox : checkboxList) {
            if (checkBox.isSelected()) {
                libraries.add(Paths.get(checkBox.getText()));
            }
        }
        return libraries;
    }

    private void chooseDirectory() {
        FileChooser chooser = new FileChooser();

        if (chooser.isApproved()) {
            String chosenPath = chooser.getSelectedFile().getPath();
            if (!chosenPath.isEmpty()) {
                customLibrary.setText(chosenPath);
                customLibrary.setEnabled(true);
                customLibrary.setSelected(true);
            }
        }
    }

    private JComponent createLibraryComponent(String libraryName) {
        JPanel libraryPanel = new JPanel();
        BoxLayout libraryLayout = new BoxLayout(libraryPanel, BoxLayout.X_AXIS);
        libraryPanel.setLayout(libraryLayout);

        JCheckBox checkBox = new JCheckBox(libraryName);
        libraryPanel.add(checkBox);
        libraryPanel.add(Box.createHorizontalGlue());

        checkboxList.add(checkBox);

        return libraryPanel;
    }
}
