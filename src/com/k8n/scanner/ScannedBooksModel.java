package com.k8n.scanner;

import com.k8n.data.Book;
import com.k8n.data.Person;
import com.k8n.data.Series;

import java.util.ArrayList;
import java.util.List;

public class ScannedBooksModel {

    private List<Book> books = new ArrayList<>();
    private List<BooksModelListener> listeners = new ArrayList<>();

    private List<Person> authors = new ArrayList<>();
    private List<String> titles = new ArrayList<>();
    private List<Person> translators = new ArrayList<>();
    private List<String> genres = new ArrayList<>();
    private List<Series> series = new ArrayList<>();

    void addBooks(List<Book> books) {
        this.books.addAll(books);
        updateBooksListeners();
    }

    public List<Book> getBooks() {
        return books;
    }

    int getBooksSize() {
        return books.size();
    }

    Book getBookAt(int index) {
        return books.get(index);
    }

    public List<Person> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Person> authors) {
        this.authors = authors;
    }

    public List<String> getTitles() {
        return titles;
    }

    public void setTitles(List<String> titles) {
        this.titles = titles;
    }

    public List<Person> getTranslators() {
        return translators;
    }

    public void setTranslators(List<Person> translators) {
        this.translators = translators;
    }

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    public List<Series> getSeries() {
        return series;
    }

    public void setSeries(List<Series> series) {
        this.series = series;
    }

    void addListener(BooksModelListener listener) {
        if (! listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    private void updateBooksListeners() {
        for (BooksModelListener listener : listeners) {
            listener.onBooksUpdated();
        }
    }

    public void updateVariantsListeners() {
        for (BooksModelListener listener : listeners) {
            listener.onVariantsUpdated();
        }
    }
}
