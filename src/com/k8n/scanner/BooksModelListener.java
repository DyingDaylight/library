package com.k8n.scanner;

public interface BooksModelListener {

    void onBooksUpdated();
    void onVariantsUpdated();

}
