package com.k8n.scanner;

import com.k8n.BaseView;
import com.k8n.data.Book;
import com.k8n.view.Java2sAutoComboBox;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ScannerView extends BaseView implements BooksModelListener{

    private static final String PREFIX = "scanner.window";

    private ScannerController controller;
    private ScannedBooksModel model;

    private ScannedBooksTableModel tableModel;

    private JTable table;
    private JProgressBar progressBar;
    private JButton cancelButton;
    private JButton saveButton;

    private Java2sAutoComboBox authorsComboBox;
    private Java2sAutoComboBox titlesComboBox;
    private Java2sAutoComboBox translatorsComboBox;
    private Java2sAutoComboBox genresComboBox;
    private Java2sAutoComboBox seriesComboBox;

    ScannerView(ScannerController controller, ScannedBooksModel model) {
        super();

        this.controller = controller;

        this.model = model;
        this.model.addListener(this);

        tableModel = new ScannedBooksTableModel(this.model);

        populateFrame();
        setText();
        showFrame();
    }

    @Override
    public void onBooksUpdated() {
        tableModel.fireTableDataChanged();
    }

    @Override
    public void onVariantsUpdated() {
        authorsComboBox.setDataList(model.getAuthors());
        titlesComboBox.setDataList(model.getTitles());
        translatorsComboBox.setDataList(model.getTranslators());
        genresComboBox.setDataList(model.getGenres());
        seriesComboBox.setDataList(model.getSeries());
    }

    @Override
    public String getPrefix() {
        return PREFIX;
    }

    public void startScan() {
        assert SwingUtilities.isEventDispatchThread();

        enableProgressBar(true);
        enableCancelButton(true);
        enableSaveButton(false);
    }

    public void endScan() {
        assert SwingUtilities.isEventDispatchThread();

        enableProgressBar(false);
        enableCancelButton(false);
        enableSaveButton(true);
    }

    @Override
    protected void populateFrame() {
        Dimension gapDimension = new Dimension(GAP, GAP);

        // Create main panel
        BorderLayout borderLayout = new BorderLayout();
        borderLayout.setVgap(GAP);

        JPanel panel = new JPanel(borderLayout);
        panel.setBorder(new EmptyBorder(GAP, GAP, GAP, GAP));

        // Create a panel with the table
        table = new JTable(tableModel);
        JScrollPane scrollPane = new JScrollPane(table);
        panel.add(scrollPane, BorderLayout.CENTER);

        authorsComboBox = new Java2sAutoComboBox(Collections.emptyList());
        authorsComboBox.setStrict(false);
        TableColumn column = table.getColumnModel().getColumn(Arrays.binarySearch(ScannedBooksTableModel.Columns.values(), ScannedBooksTableModel.Columns.AUTHOR));
        column.setCellEditor(new DefaultCellEditor(authorsComboBox));

        titlesComboBox = new Java2sAutoComboBox(Collections.emptyList());
        titlesComboBox.setStrict(false);
        column = table.getColumnModel().getColumn(Arrays.binarySearch(ScannedBooksTableModel.Columns.values(), ScannedBooksTableModel.Columns.TITLE));
        column.setCellEditor(new DefaultCellEditor(titlesComboBox));

        translatorsComboBox = new Java2sAutoComboBox(Collections.emptyList());
        translatorsComboBox.setStrict(false);
        column = table.getColumnModel().getColumn(Arrays.binarySearch(ScannedBooksTableModel.Columns.values(), ScannedBooksTableModel.Columns.TRANSLATOR));
        column.setCellEditor(new DefaultCellEditor(translatorsComboBox));

        genresComboBox = new Java2sAutoComboBox(Collections.emptyList());
        genresComboBox.setStrict(false);
        column = table.getColumnModel().getColumn(Arrays.binarySearch(ScannedBooksTableModel.Columns.values(), ScannedBooksTableModel.Columns.GENRE));
        column.setCellEditor(new DefaultCellEditor(genresComboBox));

        seriesComboBox = new Java2sAutoComboBox(Collections.emptyList());
        seriesComboBox.setStrict(false);
        column = table.getColumnModel().getColumn(Arrays.binarySearch(ScannedBooksTableModel.Columns.values(), ScannedBooksTableModel.Columns.SERIES));
        column.setCellEditor(new DefaultCellEditor(seriesComboBox));

        // Create a panel with control buttons
        JPanel controlPanel = new JPanel();
        BoxLayout boxLayout = new BoxLayout(controlPanel, BoxLayout.X_AXIS);
        controlPanel.setLayout(boxLayout);

        progressBar = new JProgressBar();
        controlPanel.add(progressBar);

        controlPanel.add(Box.createRigidArea(gapDimension));

        cancelButton = new JButton();
        controlPanel.add(cancelButton);
        cancelButton.addActionListener(e -> controller.cancelScanner());

        controlPanel.add(Box.createRigidArea(gapDimension));

        saveButton = new JButton();
        controlPanel.add(saveButton);
        saveButton.addActionListener(e -> controller.saveChecked());

        controlPanel.add(Box.createHorizontalGlue());

        panel.add(controlPanel, BorderLayout.SOUTH);

        // add main panel to the frame
        add(panel);
    }

    @Override
    protected void setText() {
        assert SwingUtilities.isEventDispatchThread();

        setTitle(resources.getGuiString("title"));

        cancelButton.setText(resources.getGuiString("cancel"));
        saveButton.setText(resources.getGuiString("save"));
    }

    private void enableProgressBar(boolean enabled) {
        progressBar.setEnabled(enabled);
        progressBar.setIndeterminate(enabled);
    }

    private void enableCancelButton(boolean enabled) {
        cancelButton.setEnabled(enabled);
    }

    private void enableSaveButton(boolean enabled) {
        saveButton.setVisible(enabled);
    }

    public List<Book> getCheckedBooks() {
        return tableModel.getCheckedBooks();
    }
}
