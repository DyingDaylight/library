package com.k8n.data;

public class Book {

    private static final Book emptyBook = new Book();

    private int id = -1;
    private Person author = new Person();
    private String genre = "";
    private String title = "";
    private String date = "";
    private Person translator = new Person();
    private Series series = null;
    private int numInSeries = 0;
    private String path = "";

    public static Book getEmptyBook() {
        return emptyBook;
    }

    public void setTitle(String title) {
        if (title != null) {
            this.title = title;
        }
    }

    public void setGenre(String genre) {
        if (genre != null) {
            this.genre = genre;
        }
    }

    public void setAuthor(Person author) {
        this.author = author;
    }

    public void setTranslator(Person translator) {
        this.translator = translator;
    }

    public void setDate(String date) {
        if (date != null) {
            this.date = date;
        }
    }

    public Person getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }

    public Person getTranslator() {
        return translator;
    }

    public String getGenre() {
        return genre;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (! genre.isEmpty()) {
            stringBuilder.append("[").append(genre).append("]").append(" ");
        }
        if (! author.isEmpty()) {
            stringBuilder.append(author.toString());
        }
        if (! (author.isEmpty() || title.isEmpty())) {
            stringBuilder.append(" - ");
        }
        if (! title.isEmpty()) {
            stringBuilder.append(title);
        }
        if (! date.isEmpty()) {
            stringBuilder.append(" (").append(date).append(")");
        }
        if (! translator.isEmpty()) {
            stringBuilder.append(" {");
            stringBuilder.append(translator.toString());
            stringBuilder.append("}");
        }

        return stringBuilder.toString();
    }

    public boolean isEmpty() {
        return title.isEmpty() || author.isEmpty();
    }

    public Series getSeries() {
        return series;
    }

    public String getPath() {
        return path;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumIsSeries() {
        return numInSeries;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
