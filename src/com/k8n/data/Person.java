package com.k8n.data;

public class Person {

    private int id = -1;
    private String firstName = "";
    private String middleName = "";
    private String lastName = "";

    public static Person parseString(String value) {
        Person person = new Person();
        if (value == null || value.isEmpty()) {
            return person;
        }

        if (value.matches("\\d+")) {
            return person;
        }

        value = value.trim();
        String[] nameParts = value.split(" ");
        if (nameParts.length < 2) {
            person.setLastName(value);
            return person;
        }

        person.setFirstName(nameParts[0]);
        person.setLastName(nameParts[nameParts.length - 1]);
        if (nameParts.length > 2) {
            int firstIndex = value.indexOf(" ") + 1;
            int lastIndex = value.lastIndexOf(" ");
            person.setMiddleName(value.substring(firstIndex, lastIndex).trim());
        }

        return person;
    }

    public void setFirstName(String firstName) {
        if (firstName != null) {
            this.firstName = firstName;
        }
    }

    public void setMiddleName(String middleName) {
        if (middleName != null) {
            this.middleName = middleName;
        }
    }

    public void setLastName(String lastName) {
        if (lastName != null) {
            this.lastName = lastName;
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (! firstName.isEmpty()) {
            stringBuilder.append(firstName).append(" ");
        }
        if (! middleName.isEmpty()) {
            stringBuilder.append(middleName).append(" ");
        }
        stringBuilder.append(lastName);
        return stringBuilder.toString();
    }

    public boolean isEmpty() {
        return lastName.isEmpty();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
