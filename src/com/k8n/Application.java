package com.k8n;

import com.k8n.manager.DBManager;

import java.util.logging.Logger;

public class Application {

    public static void main(String... args) {
        Logger.getGlobal().info("The application has started!");

        DBManager dbManager = DBManager.getInstance();
        dbManager.createDatabase();
        dbManager.createTables();

        LibraryController controller = new LibraryController();
        controller.showWindow();
    }
}
