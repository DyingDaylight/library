package com.k8n.resource;

public interface LanguageListener {
    void onLanguageUpdated();
}
