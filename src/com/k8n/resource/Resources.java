package com.k8n.resource;

import javax.swing.*;
import java.util.*;

public class Resources {

    public static String ICON_PATH_PLUS = "res/icons/icon_plus.png";
    public static String ICON_PATH_MINUS = "res/icons/icon_minus.png";
    public static String ICON_PATH_EDIT = "res/icons/icon_pen.png";

    private static String STRING_GUI = "strings/gui";

    private static Resources instance;

    private Locale locale = Locale.getDefault();
    private ResourceBundle.Control control;
    private ResourceBundle guiResourceBundle;

    private List<LanguageListener> listeners;

    private Resources() {
        control = new UTF8Control();
        listeners = new ArrayList<>();
    }

    public static Resources getInstance() {
        if (instance == null) {
            instance = new Resources();
        }
        return instance;
    }

    public String getGuiString(String key) {
        if (guiResourceBundle == null) {
            guiResourceBundle = ResourceBundle.getBundle(STRING_GUI, locale, control);
        }

        return guiResourceBundle.getString(key);
    }

    public Collection<Locale> getAvailableLocales() {
        return new ArrayList<Locale>() {{
            add(Locale.ENGLISH);
            add(new Locale("ru"));
        }};
    }

    public void setLanguage(String language) {
        locale = new Locale(language);
        if (guiResourceBundle != null) {
            guiResourceBundle = ResourceBundle.getBundle(STRING_GUI, locale, control);
        }
        UIManager.put("OptionPane.yesButtonText", getGuiString("OptionPane.yesButtonText"));
        UIManager.put("OptionPane.noButtonText", getGuiString("OptionPane.noButtonText"));
        UIManager.put("OptionPane.cancelButtonText", getGuiString("OptionPane.cancelButtonText"));
        notifyListeners();
    }

    public void addLanguageListener(LanguageListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public void removeLanguageListener(LanguageListener listener) {
        if (listener == null) {
            return;
        }

        int index = listeners.indexOf(listener);
        if (index >= 0) {
            listeners.remove(index);
        }
    }

    private void notifyListeners() {
        for (LanguageListener listener : listeners) {
            listener.onLanguageUpdated();
        }
    }

    public String getLanguage() {
        return locale.getLanguage();
    }
}
