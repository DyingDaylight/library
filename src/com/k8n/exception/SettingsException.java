package com.k8n.exception;

public class SettingsException extends Exception{


    public SettingsException(String message) {
        super(message);
    }
}
