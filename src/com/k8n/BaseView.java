package com.k8n;

import com.k8n.manager.PropertiesManager;
import com.k8n.resource.LanguageListener;
import com.k8n.resource.Resources;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;

/**
 * Abstract Frame to get PropertiesManager, Resources and handle window properties and error messages.
 */
public abstract class BaseView extends JFrame implements LanguageListener {

    private final String TOP = getPrefix() + ".top"; // a name of the window's top coordinate property
    private final String LEFT = getPrefix() + ".left";
    private final String WIDTH = getPrefix() + ".width";
    private final String HEIGHT = getPrefix() + ".height";
    private final String STATE = getPrefix() + ".extended.state";

    protected final int GAP = 10;
    protected final int Y_GAP = 5;

    protected PropertiesManager propertiesManager;
    protected Resources resources;

    public BaseView() {
        propertiesManager = PropertiesManager.getInstance();

        resources = Resources.getInstance();
        resources.setLanguage(propertiesManager.getLanguage());
        resources.addLanguageListener(this);
    }

    public final void showError(String message) {
        assert SwingUtilities.isEventDispatchThread();

        JOptionPane.showMessageDialog(this,
                message, resources.getGuiString("error"),
                JOptionPane.ERROR_MESSAGE);
    }

    @Override
    public void onLanguageUpdated() {
        setText();
    }

    public abstract String getPrefix();

    public HashMap<String, String> getStateMemento() {
        HashMap<String, String> state = new HashMap<>();
        state.put(TOP, String.valueOf(getBounds().y));
        state.put(LEFT, String.valueOf(getBounds().x));
        state.put(WIDTH, String.valueOf(getBounds().width));
        state.put(HEIGHT, String.valueOf(getBounds().height));
        state.put(STATE, String.valueOf(getExtendedState()));
        return state;
    }

    public void setStateMemento(HashMap<String, String> state) {
        int x = 10;
        int y = 10;
        int width = 300;
        int height = 200;
        int externalState = NORMAL;

        if (state.containsKey(TOP)) {
            y = Integer.parseInt(state.get(TOP));
        }

        if (state.containsKey(LEFT)) {
            x = Integer.parseInt(state.get(LEFT));
        }

        if (state.containsKey(WIDTH)) {
            width = Integer.parseInt(state.get(WIDTH));
        }

        if (state.containsKey(HEIGHT)) {
            height = Integer.parseInt(state.get(HEIGHT));
        }

        if (state.containsKey(STATE)) {
            externalState = Integer.parseInt(state.get(STATE));
            if (externalState == ICONIFIED) {
                externalState = NORMAL;
            }
        }

        setBounds(x, y, width, height);
        setExtendedState(externalState);
    }

    protected abstract void populateFrame();
    protected abstract void setText();

    protected void showFrame() {
        assert SwingUtilities.isEventDispatchThread();

        setTitle(resources.getGuiString("title"));
        setStateMemento(propertiesManager.getProperties(getPrefix()));
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setVisible(true);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                closeFrame();
            }
        });
    }

    protected final void closeFrame() {
        performPreCloseActions();
        performCloseAction();
    }

    protected void performPreCloseActions() {
        HashMap<String, String> stateMemento = getStateMemento();
        propertiesManager.addProperties(stateMemento);
        propertiesManager.save();
    }

    protected void performCloseAction() {
        assert SwingUtilities.isEventDispatchThread();

        setVisible(false);
        dispose();
    }
}
