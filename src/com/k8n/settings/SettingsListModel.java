package com.k8n.settings;

import com.k8n.settings.settingsItem.SettingsItemController;

import javax.swing.*;
import javax.swing.event.ListDataListener;

public class SettingsListModel extends DefaultListModel<SettingsItemController> {

    private SettingsModel model;

    public SettingsListModel(SettingsModel model) {
        this.model = model;
    }

    @Override
    public int getSize() {
        return model.getSize();
    }

    @Override
    public SettingsItemController getElementAt(int index) {
        return model.getItemAt(index);
    }

    @Override
    public void addListDataListener(ListDataListener l) {

    }

    @Override
    public void removeListDataListener(ListDataListener l) {

    }
}
