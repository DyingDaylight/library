package com.k8n.settings;

import com.k8n.exception.SettingsException;
import com.k8n.settings.settingsItem.SettingsItemController;

public class SettingsController {

    private SettingsView view;
    private SettingsModel model;

    public SettingsController() {
        model = new SettingsModel();
        view = new SettingsView(this, model);
    }


    public void resetSettings() {
        for (int i = 0; i < model.getSize(); i++) {
            SettingsItemController item = model.getItemAt(i);
            item.resetData();
        }
    }

    public void saveSettings() {
        for (int i = 0; i < model.getSize(); i++) {
            SettingsItemController item = model.getItemAt(i);
            try {
                item.saveData();
            } catch (SettingsException e) {
                view.showError(e.getMessage());
            }
        }
    }

    public void itemSelected(SettingsItemController settingsItem) {
        try {
            settingsItem.loadData();
        } catch (SettingsException e) {
            view.showError(e.getMessage());
        }
    }
}
