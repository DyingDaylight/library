package com.k8n.settings;

import com.k8n.BaseView;
import com.k8n.settings.settingsItem.SettingsItemController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class SettingsView extends BaseView {

    private static final String PREFIX = "settings.window";

    private SettingsController controller;
    private SettingsModel model;

    private ListModel<SettingsItemController> listModel;

    private JList<SettingsItemController> list;
    private JLabel settingsItemName;
    private JButton saveButton;
    private JButton cancelButton;
    private JButton applyButton;
    private JButton resetButton;
    private JPanel contentContainer;

    SettingsView(SettingsController controller, SettingsModel model) {
        super();

        this.controller = controller;
        this.model = model;

        listModel = new SettingsListModel(model);

        populateFrame();
        setText();
        showFrame();
    }

    @Override
    public String getPrefix() {
        return PREFIX;
    }

    @Override
    protected void populateFrame() {
        BorderLayout mainPanelBorderLayout = new BorderLayout();
        JPanel mainPanel = new JPanel(mainPanelBorderLayout);

        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);

        list = new JList<>(listModel);
        list.setSelectedIndex(0);
        JScrollPane scrollPane = new JScrollPane(list);
        splitPane.add(scrollPane);
        splitPane.setDividerSize(3);
        splitPane.setResizeWeight(0.3);

        SettingsListCellRenderer listCellRenderer = new SettingsListCellRenderer();
        list.setCellRenderer(listCellRenderer);

        list.addListSelectionListener(e -> {
            SettingsItemController currentItem = list.getSelectedValue();
            contentContainer.remove(((BorderLayout) contentContainer.getLayout()).getLayoutComponent(BorderLayout.CENTER));
            contentContainer.add(currentItem.getView(), BorderLayout.CENTER);
            contentContainer.repaint();
            settingsItemName.setText(currentItem.getName());
            controller.itemSelected(currentItem);
        });

        JPanel contentPanel = new JPanel(new BorderLayout());
        contentPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, GAP));

        JPanel itemTitlePanel = new JPanel();
        BoxLayout titleBoxLayout = new BoxLayout(itemTitlePanel, BoxLayout.X_AXIS);
        itemTitlePanel.setLayout(titleBoxLayout);
        itemTitlePanel.setBorder(new EmptyBorder(Y_GAP, GAP, Y_GAP, 0));

        settingsItemName = new JLabel();
        itemTitlePanel.add(settingsItemName);

        itemTitlePanel.add(Box.createHorizontalGlue());

        resetButton = new JButton();
        itemTitlePanel.add(resetButton);
        resetButton.addActionListener(e -> {
            controller.resetSettings();
        });

        contentPanel.add(itemTitlePanel, BorderLayout.NORTH);

        SettingsItemController settingsItem = model.getItemAt(0);
        contentContainer = new JPanel(new BorderLayout());
        JPanel itemPanel = settingsItem.getView();
        contentContainer.add(itemPanel, BorderLayout.CENTER);
        contentPanel.add(contentContainer);
        controller.itemSelected(settingsItem);

        splitPane.add(contentPanel);
        mainPanel.add(splitPane, BorderLayout.CENTER);

        JPanel controlPanel = new JPanel();
        BoxLayout boxLayout = new BoxLayout(controlPanel, BoxLayout.X_AXIS);
        controlPanel.setLayout(boxLayout);
        controlPanel.setBorder(new EmptyBorder(Y_GAP, GAP, Y_GAP, GAP));

        controlPanel.add(Box.createHorizontalGlue());

        Dimension gapDimension = new Dimension(GAP, GAP);

        saveButton = new JButton();
        controlPanel.add(saveButton);
        saveButton.addActionListener(e -> {
            controller.saveSettings();
            closeFrame();
        });

        controlPanel.add(Box.createRigidArea(gapDimension));

        cancelButton = new JButton();
        controlPanel.add(cancelButton);
        cancelButton.addActionListener(e -> {
            closeFrame();
        });

        controlPanel.add(Box.createRigidArea(gapDimension));

        applyButton = new JButton();
        controlPanel.add(applyButton);
        applyButton.addActionListener(e -> {
            controller.saveSettings();
        });

        mainPanel.add(controlPanel, BorderLayout.SOUTH);

        add(mainPanel);
    }

    @Override
    protected void setText() {
        list.invalidate();

        SettingsItemController currentItem = list.getSelectedValue();
        currentItem.updateLanguage();

        settingsItemName.setText(currentItem.getName());

        saveButton.setText(resources.getGuiString("save"));
        cancelButton.setText(resources.getGuiString("cancel"));
        applyButton.setText(resources.getGuiString("apply"));
        resetButton.setText(resources.getGuiString("reset"));
    }
}
