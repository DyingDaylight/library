package com.k8n.settings.settingsItem;

import com.k8n.resource.Resources;
import com.k8n.view.IconButton;

import javax.swing.*;
import java.awt.*;

public class SettingsItemListView extends JPanel implements SettingsItemListener{

    private SettingsItemController controller;
    private SettingsItemModel model;
    private SettingsItemListModel<String> listModel;

    private JButton minusButton;
    private JButton editButton;

    public SettingsItemListView(SettingsItemController controller, SettingsItemModel model) {
        super(new BorderLayout());

        this.controller = controller;

        this.model = model;
        this.model.addListener(this);

       listModel = new SettingsItemListModel<>();

        JList<String> list = new JList<>();
        list.setModel(listModel);
        list.addListSelectionListener(e -> {
            if (e.getFirstIndex() >= 0) {
                minusButton.setEnabled(true);
                editButton.setEnabled(true);
            }
        });

        JScrollPane scrollPane = new JScrollPane(list);
        add(scrollPane, BorderLayout.CENTER);

        JPanel controlPanel = new JPanel();
        BoxLayout boxLayout = new BoxLayout(controlPanel, BoxLayout.Y_AXIS);
        controlPanel.setLayout(boxLayout);
        controlPanel.setBorder(BorderFactory.createEmptyBorder(0, 5, 5,0));
        add(controlPanel, BorderLayout.EAST);

        JButton addButton = new IconButton(Resources.ICON_PATH_PLUS);
        controlPanel.add(addButton);
        addButton.addActionListener(e -> {
            String newItem = this.controller.addItem();
            if (newItem != null && !newItem.isEmpty()) {
                listModel.addElement(newItem);
            }
        });

        Dimension verticalGap = new Dimension(1, 5);

        controlPanel.add(Box.createRigidArea(verticalGap));

        minusButton = new IconButton(Resources.ICON_PATH_MINUS);
        minusButton.setEnabled(false);
        controlPanel.add(minusButton);
        minusButton.addActionListener(e -> {
            listModel.remove(list.getSelectedIndex());
        });

        controlPanel.add(Box.createRigidArea(verticalGap));

        editButton = new IconButton(Resources.ICON_PATH_EDIT);
        editButton.setEnabled(false);
        controlPanel.add(editButton);
        editButton.addActionListener(e -> {
            String newItem = this.controller.addItem();
            if (newItem != null && !newItem.isEmpty()) {
                int index = list.getSelectedIndex();
                listModel.remove(index);
                listModel.add(index, newItem);
            }
        });
    }

    @Override
    public void settingsItemUpdated() {
        listModel.addItems(model.getItems());
    }

    public String[] getListItems() {
        String[] items = new String[listModel.getSize()];
        for (int i = 0; i < listModel.getSize(); i++) {
            items[i] = listModel.get(i);
        }
        return items;
    }
}
