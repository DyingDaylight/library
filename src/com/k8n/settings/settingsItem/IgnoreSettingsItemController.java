package com.k8n.settings.settingsItem;

import com.k8n.exception.SettingsException;
import com.k8n.manager.SettingsManager;
import com.k8n.resource.Resources;

import javax.swing.*;
import java.util.HashMap;

public class IgnoreSettingsItemController implements SettingsItemController {

    public static final String PATHS = "ignore.paths";

    private SettingsItemListView view;
    private SettingsItemModel model;

    public IgnoreSettingsItemController() {
        model = new SettingsItemModel();
    }

    @Override
    public void updateLanguage() {

    }

    @Override
    public String getName() {
        Resources resources = Resources.getInstance();
        return resources.getGuiString("settings.item.ignore");
    }

    @Override
    public JPanel getView() {
        if (view == null) {
            view = new SettingsItemListView(this, model);
        }
        return view;
    }

    @Override
    public String addItem() {
        return JOptionPane.showInputDialog(view, Resources.getInstance().getGuiString("settings.ignore.input.message"),
                Resources.getInstance().getGuiString("settings.ignore.input.title"), JOptionPane.INFORMATION_MESSAGE);
    }

    public void loadData() throws SettingsException {
        SettingsManager manager = SettingsManager.getInstance();
        String[] items = manager.getArraySetting(PATHS);
        model.setItems(items);
    }

    public void resetData() {
        model.notifyListeners();
    }

    @Override
    public void saveData() throws SettingsException {
        if (view == null) {
            return;
        }
        HashMap<String, Object> settings = new HashMap<>();
        settings.put(PATHS, view.getListItems());
        SettingsManager manager = SettingsManager.getInstance();
        manager.updateSettings(settings);
        manager.save();
    }
}
