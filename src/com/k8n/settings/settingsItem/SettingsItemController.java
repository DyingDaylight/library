package com.k8n.settings.settingsItem;

import com.k8n.exception.SettingsException;

import javax.swing.*;

public interface SettingsItemController {

    void updateLanguage();

    String getName();

    JPanel getView();

    String addItem();

    void loadData() throws SettingsException;

    void resetData();

    void saveData() throws SettingsException;
}
