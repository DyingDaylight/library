package com.k8n.settings.settingsItem;

public interface SettingsItemListener {

    void settingsItemUpdated();
}
