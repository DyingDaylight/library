package com.k8n.settings.settingsItem;

import com.k8n.exception.SettingsException;
import com.k8n.manager.SettingsManager;
import com.k8n.resource.Resources;
import com.k8n.view.FileChooser;

import javax.swing.*;
import java.util.HashMap;

public class LibrariesSettingsItemController implements SettingsItemController {

    private static final String PATHS = "library.paths";

    private SettingsItemListView view;
    private SettingsItemModel model;

    public LibrariesSettingsItemController() {
        model = new SettingsItemModel();
    }

    @Override
    public void updateLanguage() {

    }

    @Override
    public String getName() {
        Resources resources = Resources.getInstance();
        return resources.getGuiString("settings.item.libraries");
    }

    @Override
    public JPanel getView() {
        if (view == null) {
            view = new SettingsItemListView(this, model);
        }
        return view;
    }

    @Override
    public String addItem() {
        FileChooser chooser = new FileChooser();
        if (chooser.isApproved()) {
            String chosenPath = chooser.getSelectedFile().getPath();
            if (!chosenPath.isEmpty()) {
                return chosenPath;
            }
        }
        return null;
    }

    public void loadData() throws SettingsException {
        SettingsManager manager = SettingsManager.getInstance();
        String[] items = manager.getArraySetting(PATHS);
        model.setItems(items);
    }

    public void resetData() {
        model.notifyListeners();
    }

    @Override
    public void saveData() throws SettingsException {
        HashMap<String, Object> settings = new HashMap<>();
        settings.put(PATHS, view.getListItems());
        SettingsManager manager = SettingsManager.getInstance();
        manager.updateSettings(settings);
        manager.save();
    }
}
