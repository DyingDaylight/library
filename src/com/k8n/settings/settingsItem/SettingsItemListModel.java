package com.k8n.settings.settingsItem;

import javax.swing.*;

public class SettingsItemListModel<T> extends DefaultListModel<T> {

    public SettingsItemListModel() {
    }

    public void addItems(T[] items) {
        removeAllElements();
        for (T item : items) {
            addElement(item);
        }
    }
}
