package com.k8n.settings.settingsItem;

import java.util.ArrayList;
import java.util.List;

public class SettingsItemModel {

    private String[] items;

    private List<SettingsItemListener> listeners;

    public SettingsItemModel() {
        items = new String[0];
        listeners = new ArrayList<>();
    }

    public void setItems(String[] items) {
        this.items = items;
        notifyListeners();
    }

    public String[] getItems() {
        return items;
    }

    public void addListener(SettingsItemListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public void notifyListeners() {
        for (SettingsItemListener listener : listeners) {
            listener.settingsItemUpdated();
        }
    }
}
