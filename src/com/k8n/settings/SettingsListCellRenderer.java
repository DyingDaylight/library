package com.k8n.settings;

import com.k8n.settings.settingsItem.SettingsItemController;

import javax.swing.*;
import java.awt.*;

public class SettingsListCellRenderer extends DefaultListCellRenderer {

    @Override
    public Component getListCellRendererComponent(
            JList<?> list,
            Object value,
            int index,
            boolean isSelected,
            boolean cellHasFocus)
    {
        SettingsItemController settingsItem = (SettingsItemController) value;
        return super.getListCellRendererComponent(list, settingsItem.getName(), index, isSelected, cellHasFocus);
    }
}
