package com.k8n.settings;

import com.k8n.settings.settingsItem.IgnoreSettingsItemController;
import com.k8n.settings.settingsItem.LibrariesSettingsItemController;
import com.k8n.settings.settingsItem.SettingsItemController;

import java.util.ArrayList;
import java.util.List;

public class SettingsModel {

    private List<SettingsItemController> settingsItems = new ArrayList<>();

    public SettingsModel() {
        settingsItems.add(new LibrariesSettingsItemController());
        settingsItems.add(new IgnoreSettingsItemController());
    }

    public int getSize() {
        return settingsItems.size();
    }

    public SettingsItemController getItemAt(int index) {
        return settingsItems.get(index);
    }
}
