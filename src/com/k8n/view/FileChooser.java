package com.k8n.view;

import com.k8n.manager.PropertiesManager;
import com.k8n.resource.Resources;

import javax.swing.*;
import java.io.File;
import java.util.HashMap;

public class FileChooser extends JFileChooser {

    private static final String LAST_PATH = "last.path";

    private String path;

    public FileChooser() {
        setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        path = PropertiesManager.getInstance().getProperties(LAST_PATH).get(LAST_PATH);
    }

    public FileChooser(String initialPath) {
        setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        this.path = initialPath;
    }

    public boolean isApproved() {
        if (path != null && !path.isEmpty()) {
            setCurrentDirectory(new File(path));
        } else {
            setCurrentDirectory(getFileSystemView().getDefaultDirectory());
        }

        setText();

        Resources resources = Resources.getInstance();
        int result = showDialog(this, resources.getGuiString("choose"));
        boolean isApproved = (result == JFileChooser.APPROVE_OPTION);

        if (isApproved) {
            path = getSelectedFile().getPath();
            HashMap<String, String> state = new HashMap<>();
            state.put(LAST_PATH, path);
            PropertiesManager.getInstance().addProperties(state);
        }

        return isApproved;
    }

    private void setText() {
        Resources resources = Resources.getInstance();
        UIManager.put("FileChooser.openDialogTitleText", resources.getGuiString("FileChooser.openDialogTitleText"));
        UIManager.put("FileChooser.saveDialogTitleText", resources.getGuiString("FileChooser.saveDialogTitleText"));
        UIManager.put("FileChooser.lookInLabelText", resources.getGuiString("FileChooser.lookInLabelText"));
        UIManager.put("FileChooser.saveInLabelText", resources.getGuiString("FileChooser.saveInLabelText"));
        UIManager.put("FileChooser.upFolderToolTipText", resources.getGuiString("FileChooser.upFolderToolTipText"));
        UIManager.put("FileChooser.homeFolderToolTipText", resources.getGuiString("FileChooser.homeFolderToolTipText"));
        UIManager.put("FileChooser.newFolderToolTipText", resources.getGuiString("FileChooser.newFolderToolTipText"));
        UIManager.put("FileChooser.listViewButtonToolTipText", resources.getGuiString("FileChooser.listViewButtonToolTipText"));
        UIManager.put("FileChooser.detailsViewButtonToolTipText", resources.getGuiString("FileChooser.detailsViewButtonToolTipText"));
        UIManager.put("FileChooser.fileNameHeaderText", resources.getGuiString("FileChooser.fileNameHeaderText"));
        UIManager.put("FileChooser.fileSizeHeaderText", resources.getGuiString("FileChooser.fileSizeHeaderText"));
        UIManager.put("FileChooser.fileTypeHeaderText", resources.getGuiString("FileChooser.fileTypeHeaderText"));
        UIManager.put("FileChooser.fileDateHeaderText", resources.getGuiString("FileChooser.fileDateHeaderText"));
        UIManager.put("FileChooser.fileAttrHeaderText", resources.getGuiString("FileChooser.fileAttrHeaderText"));
        UIManager.put("FileChooser.fileNameLabelText", resources.getGuiString("FileChooser.fileNameLabelText"));
        UIManager.put("FileChooser.folderNameLabelText", resources.getGuiString("FileChooser.folderNameLabelText"));
        UIManager.put("FileChooser.filesOfTypeLabelText", resources.getGuiString("FileChooser.filesOfTypeLabelText"));
        UIManager.put("FileChooser.openButtonText", resources.getGuiString("FileChooser.openButtonText"));
        UIManager.put("FileChooser.openButtonToolTipText", resources.getGuiString("FileChooser.openButtonToolTipText"));
        UIManager.put("FileChooser.saveButtonText", resources.getGuiString("FileChooser.saveButtonText"));
        UIManager.put("FileChooser.saveButtonToolTipText", resources.getGuiString("FileChooser.saveButtonToolTipText"));
        UIManager.put("FileChooser.directoryOpenButtonText", resources.getGuiString("FileChooser.directoryOpenButtonText"));
        UIManager.put("FileChooser.directoryOpenButtonToolTipText", resources.getGuiString("FileChooser.directoryOpenButtonToolTipText"));
        UIManager.put("FileChooser.cancelButtonText", resources.getGuiString("FileChooser.cancelButtonText"));
        UIManager.put("FileChooser.cancelButtonToolTipText", resources.getGuiString("FileChooser.cancelButtonToolTipText"));
        UIManager.put("FileChooser.newFolderErrorText", resources.getGuiString("FileChooser.newFolderErrorText"));
        UIManager.put("FileChooser.acceptAllFileFilterText", resources.getGuiString("FileChooser.acceptAllFileFilterText"));
        UIManager.put("OptionPane.yesButtonText", resources.getGuiString("OptionPane.yesButtonText"));
        UIManager.put("OptionPane.noButtonText", resources.getGuiString("OptionPane.noButtonText"));
        UIManager.put("OptionPane.cancelButtonText", resources.getGuiString("OptionPane.cancelButtonText"));
        UIManager.put("ProgressMonitor.progressText", resources.getGuiString("ProgressMonitor.progressText"));
        SwingUtilities.updateComponentTreeUI(this);
    }


}
