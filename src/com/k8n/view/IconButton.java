package com.k8n.view;

import javax.swing.*;

public class IconButton extends JButton {

    public IconButton(String iconPath) {
        super.setBorder(null);

        ImageIcon icon = new ImageIcon(iconPath);
        super.setIcon(icon);
    }
}
