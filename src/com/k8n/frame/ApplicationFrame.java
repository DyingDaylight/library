package com.k8n.frame;

import com.k8n.BaseView;
import com.k8n.LibraryController;
import com.k8n.manager.PropertiesManager;
import com.k8n.resource.Resources;
import com.k8n.view.FileChooser;

import javax.swing.*;
import java.awt.event.WindowEvent;
import java.util.Locale;

public class ApplicationFrame extends BaseView {

    private static final String PREFIX = "main.window";

    private JMenu fileMenu;
    private JMenu viewMenu;

    private JMenuItem settingsMenu;
    private JMenuItem scanMenu;
    private JMenuItem exitMenu;
    private JMenu languageMenu;

    private FileChooser chooser;

    private LibraryController controller;

    public ApplicationFrame(LibraryController controller) {
        super();

        this.controller = controller;

        populateFrame();
        setText();
        showFrame();
    }

    public String toString() {
        return "Library Application Main Frame";
    }

    private void populateLanguageMenu(JMenu languageMenu) {
        ButtonGroup buttonGroup = new ButtonGroup();

        for (Locale locale : Resources.getInstance().getAvailableLocales()) {
            JRadioButtonMenuItem menuItem = new JRadioButtonMenuItem(locale.getDisplayLanguage(locale));
            menuItem.addActionListener(event -> {
                resources.setLanguage(locale.getLanguage());
                propertiesManager.setLanguage(resources.getLanguage());
                propertiesManager.save();
            });
            buttonGroup.add(menuItem);
            languageMenu.add(menuItem);

            if (PropertiesManager.getInstance().getLanguage().equals(locale.getLanguage())) {
                menuItem.setSelected(true);
            }
        }
    }

    @Override
    protected void setText() {
        Resources resources = Resources.getInstance();

        setTitle(resources.getGuiString("title"));

        fileMenu.setText(resources.getGuiString("menu.file"));
        scanMenu.setText(resources.getGuiString("menu.file.scan"));
        settingsMenu.setText(resources.getGuiString("menu.file.settings"));
        exitMenu.setText(resources.getGuiString("menu.file.exit"));
        viewMenu.setText(resources.getGuiString("menu.view"));
        languageMenu.setText(resources.getGuiString("menu.view.language"));
    }

    @Override
    public String getPrefix() {
        return PREFIX;
    }

    @Override
    protected void populateFrame() {
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        fileMenu = new JMenu();
        menuBar.add(fileMenu);

        scanMenu = new JMenuItem();
        fileMenu.add(scanMenu);
        scanMenu.addActionListener(event -> {
            controller.scan();
        });

        fileMenu.addSeparator();

        settingsMenu = new JMenuItem();
        fileMenu.add(settingsMenu);
        settingsMenu.addActionListener(event -> controller.openSettings());

        fileMenu.addSeparator();

        exitMenu = new JMenuItem();
        fileMenu.add(exitMenu);
        exitMenu.addActionListener(event -> dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING)));

        viewMenu = new JMenu();
        menuBar.add(viewMenu);

        languageMenu = new JMenu();
        viewMenu.add(languageMenu);

        populateLanguageMenu(languageMenu);
    }

    @Override
    protected void performPreCloseActions() {
        int result = JOptionPane.showConfirmDialog(this, resources.getGuiString("exit.confirm"),
                resources.getGuiString("exit.title"), JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            super.performPreCloseActions();
        }
    }

    @Override
    protected void performCloseAction() {
        System.exit(0);
    }
}
