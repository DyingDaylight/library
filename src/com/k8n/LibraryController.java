package com.k8n;

import com.k8n.frame.ApplicationFrame;
import com.k8n.manager.PropertiesManager;
import com.k8n.resource.Resources;
import com.k8n.scanner.settings.ScannerSettingsController;
import com.k8n.settings.SettingsController;

import javax.swing.*;
import java.awt.*;

public class LibraryController {

    private PropertiesManager propertiesManager;
    private Resources resources;

    private ApplicationFrame view;

    public LibraryController() {
        propertiesManager = PropertiesManager.getInstance();

        resources = Resources.getInstance();
        resources.setLanguage(propertiesManager.getLanguage());
    }

    public void showWindow() {
        EventQueue.invokeLater(() -> {
            assert SwingUtilities.isEventDispatchThread();

            view = new ApplicationFrame(this);
        });
    }

    public void openSettings() {
        SettingsController settingsController = new SettingsController();
    }

    public void scan() {
        ScannerSettingsController scannerSettingsController = new ScannerSettingsController();
    }

}
